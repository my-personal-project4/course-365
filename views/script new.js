var vObjectCourses = []
// ham load trang
$(document).ready(function () {
  onPageLoading();

});
// onpageloading
function onPageLoading() {
  callApiGetServer();
  getPopularCourse(vObjectCourses);
  getTrendingCourse(vObjectCourses);
}
// call api
function callApiGetServer() {
  $.ajax({
    url: "/courses",
    type: "GET",
    dataType: "json",
    async: false,
    success: function (responseJson) {
      console.log(responseJson);
      vObjectCourses = responseJson.courses;
    },
    error: function (ajaxContent) {
      console.log(ajaxContent);
    }
  })
}
    // REGION 4
    function getPopularCourse(paramListCourses) {
        var vListArr = [];
        vListArr = paramListCourses.filter(function(element){
            return element.isPopular == true;
        })
        console.log(vListArr);
        loadDataToPopularCourses(vListArr);
    }
    function getTrendingCourse(paramListCourses) {
        var vListArr = [];
        vListArr = paramListCourses.filter(function(element){
            return element.isTrending == true;
        })
        console.log(vListArr);
        loadDataToTrendingCourses(vListArr);
    }

   
    function loadDataToPopularCourses(paramListCourses) {
        var vCardPopularCourses = $("#card-popular-courses");
        var vHtmls = paramListCourses.map(function(element){
            return `<div class="card">
            <img class="card-img-top" src=${element.coverImage} alt="Card image"
                style="width:100%">
            <div class="card-body">
                <h4 class="card-title text-primary">${element.courseName}</h4>
                <p class="card-text"><i class="fas fa-clock"></i> ${element.duration} ${element.level}</p>
                <p class="card-text"><b>$${element.price}</b> <del>$${element.discountPrice}</del></p>
            </div>
            <div class="card-footer">
                <img src= ${element.teacherPhoto} alt="" style="width: 25px"
                    class="rounded-circle">
                <span style="font-size: 14px;" class="ml-2">${element.teacherName}</span>
                <i class="far fa-bookmark float-right mt-1"></i>
            </div>
        </div>`
        })
        var vHtml=vHtmls.join('');
        vCardPopularCourses.html(vHtml);
    }
    function loadDataToTrendingCourses(paramListCourses) {
        var vCardTrendingCourses = $("#card-trending-courses");
        var vHtmls = paramListCourses.map(function(element){
            return `<div class="card">
            <img class="card-img-top" src=${element.coverImage} alt="Card image"
                style="width:100%">
            <div class="card-body">
                <h4 class="card-title text-primary">${element.courseName}</h4>
                <p class="card-text"><i class="fas fa-clock"></i> ${element.duration} ${element.level}</p>
                <p class="card-text"><b>$${element.price}</b> <del>$${element.discountPrice}</del></p>
            </div>
            <div class="card-footer">
                <img src= ${element.teacherPhoto} alt="" style="width: 25px"
                    class="rounded-circle">
                <span style="font-size: 14px;" class="ml-2">${element.teacherName}</span>
                <i class="far fa-bookmark float-right mt-1"></i>
            </div>
        </div>`
        })
        var vHtml=vHtmls.join('');
        vCardTrendingCourses.html(vHtml);
    }
