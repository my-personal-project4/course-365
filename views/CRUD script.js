const gCOURSES_COLS =  ["_id", "courseCode", "courseName", "duration", "level", "price", "discountPrice", "coverImage", "teacherName", "teacherPhoto","isPopular", "isTrending", "action"];
const gID_COL = 0;
const gCOURSE_CODE_COL = 1;
const gCOURSE_NAME_COL = 2;
const gDURATION_COL = 3;
const gLEVEL_COL = 4;
const gPRICE_COL = 5;
const gDISCOUNT_PRICE_COL = 6;
const gCOVER_IMAGE_COL = 7;
const gTEACHER_COL = 8;
const gTEACHER__PHOTO_COL = 9;
const gIS_POPULAR_COL = 10;
const gIS_TRENDING_COL = 11;
const gACTION_COL = 12;
var vObjectCourses = [];
vIdCourseDelete = "";
vIdCourseEdit = "";
vCourseCode = "";
$(document).ready(function () {
    callApiGetServer();
    onPageLoading();
    loadDataToTable(vObjectCourses);
    $("#btn-add-course").on('click', function () {
        onBtnCreateCourse();
    });
    $("#btn-create-course").on('click', function () {
        onBtnConfirmCreateCourse();
    });
    $(document).on("click", "#courses-table .update-btn", function () {
        onBtnEditClick(this)
    });
    $(document).on("click", "#courses-table .delete-btn", function () {
        onBtnDeleteClick(this)
    });
    $("#btn-update-course").on('click', function () {
        onBtnClickConfirmUpdate();
    });
    $("#btn-confirm-delete-course").on('click', function () {
        onBtnClickDeleteCourse();
    })
});
//function tải lại trang
function onPageLoading() {
    // thiết lập data table
    $("#courses-table").DataTable({
        columns: [
            { data: gCOURSES_COLS[gID_COL] },
            { data: gCOURSES_COLS[gCOURSE_CODE_COL] },
            { data: gCOURSES_COLS[gCOURSE_NAME_COL] },
            { data: gCOURSES_COLS[gDURATION_COL] },
            { data: gCOURSES_COLS[gLEVEL_COL] },
            { data: gCOURSES_COLS[gPRICE_COL] },
            { data: gCOURSES_COLS[gDISCOUNT_PRICE_COL] },
            { data: gCOURSES_COLS[gCOVER_IMAGE_COL] },
            { data: gCOURSES_COLS[gTEACHER_COL] },
            { data: gCOURSES_COLS[gTEACHER__PHOTO_COL] },
            { data: gCOURSES_COLS[gIS_POPULAR_COL] },
            { data: gCOURSES_COLS[gIS_TRENDING_COL] },
            { data: gCOURSES_COLS[gACTION_COL] }
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                className: "w-25 text-center",
                defaultContent: "<div class='row'><div class='col-6'><button class='btn btn-primary update-btn'><i class='fas fa-edit'></i> Sửa</button></div> <button class='btn btn-danger delete-btn'><i class='fas fa-eraser'></i> Xóa</button></div></div>"
            },
            {
                targets: gPRICE_COL,
                className: "text-center ",
                render: function (data, type, row, meta) {
                return "$" + data ;
                }
            },
            {
                targets: gDISCOUNT_PRICE_COL,
                className: "text-center ",
                render: function (data, type, row, meta) {
                return "$" + data ;
                }
            },
            {
                targets: gCOVER_IMAGE_COL,
                render: function(data) {
                         return '<img style = "width: 100px;" src="'+data+'">'
                        }
            },
            {
                targets: gTEACHER__PHOTO_COL,
                render: function(data) {
                         return '<img style = "width: 100px;" src="'+data+'">'
                        }
            },
        ]
    });
}
// get all data
function callApiGetServer() {
    $.ajax({
        async:false,
        url: "/courses",
        type: "GET",
        dataType: "json",
        async: false,
        success: function (responseJson) {
            console.log(responseJson);
            vObjectCourses = responseJson.courses;
            console.log(vObjectCourses);
           
            
        },
        error: function (ajaxContent) {
            console.log(ajaxContent);
        }
    })
}
// create course modal
function onBtnCreateCourse() {
    $("#create-course-modal").modal('show');
}
// comfirm create thêm mới course
function onBtnConfirmCreateCourse() {
    // tạo biến lưu thông tin từ input
    var gConfirmButton = {
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: "",
        isTrending: ""
    };
    // đọc thông tin từ input
    readDataFromCreate(gConfirmButton);
    // hàm validate
    var vCheckData = validateDataConfirm(gConfirmButton);
    // nếu validate = true thì chạy tiếp
    if (vCheckData == true) {
        // thêm data vào bảng
        callApiPostData(gConfirmButton)
        console.log(gConfirmButton)
        alert("INSERT thông tin thành công!");
        // xóa trắng bảng
        resetCreateModal();
        // ẩn modal
        $("#create-user").modal('hide');
    }
}
function callApiPostData(paramConfirmButton) {
    $.ajax({
        type: "POST",
        url: "/courses",
        data: JSON.stringify(paramConfirmButton),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (res) {
            console.log(res)
            location.reload();
        },
        error: function (res) {
            console.log(res.status)
        }
    });
}
// nút sửa modal
function onBtnEditClick(paramUpdateButton) {
    // show modal
    $("#update-course-modal").modal('show');
    //loadModal
    loadDataToUpdateModal(paramUpdateButton);
}
// function hiển thi, thay đổi course, update
function onBtnClickConfirmUpdate() {
    // biến lưu thông tin update
    var gConfirmButtonUpdate = {
        _id: "",
        courseCode: "",
        courseName: "",
        price: 0,
        discountPrice: 0,
        duration: "",
        level: "",
        coverImage: "",
        teacherName: "",
        teacherPhoto: "",
        isPopular: "",
        isTrending: ""
    };
    // đọc thông tin từ input
    readDataUpdate(gConfirmButtonUpdate);
    // hàm validate thông tin
    var vCheckData = validateUpdateConfirm(gConfirmButtonUpdate);
    // nếu thông tin nhập vào hợp lệ
    if (vCheckData) {
        // api update
        callApiUpdate(gConfirmButtonUpdate);
        // load lại bảng
        loadDataToTable(vObjectCourses);
        alert("UPDATE thông tin thành công!");
        // xóa modal
        resetUpdateModal();
        // ẩn modal
        $("#update-course-modal").modal('hide');
    }
}
// function deleta course
function onBtnDeleteClick(paramDeleteButton) {
    $("#delete-confirm-modal").modal('show');
    var vCurrentTr = $(paramDeleteButton).closest("tr");
    var vTable = $("#courses-table").DataTable();
    var vDataRow = vTable.row(vCurrentTr).data();
    vCourseCode = vDataRow.courseCode;
    vObjectCourses.filter(item => {
        if (item.courseCode === vCourseCode) {
            vIdCourseDelete = item._id;
            console.log(vIdCourseDelete);
        }
    })
}
// function delete course
function onBtnClickDeleteCourse() {
    callApiDelete();
    // load lại bảng
    loadDataToTable(vObjectCourses);
    // ẩn modal 
    $("#delete-confirm-modal").modal("hide");
}

// delete course
function callApiDelete() {
    $.ajax({
        url: "/courses/" + vIdCourseDelete,
        type: "DELETE",
        dataType: "json",
        success: function (res) {
            alert("DELETE thông tin thành công!");
            location.reload();
        },
        error: function (error) {
            alert(error.responseText);
        },
    })
}
// lấy thông tin được điền từ input
function readDataFromCreate(paramConfirmButton) {
    paramConfirmButton.courseCode = $("#input-course-code").val().trim();
    paramConfirmButton.courseName = $("#input-course-name").val().trim();
    paramConfirmButton.price = Number($("#input-price").val().trim());
    paramConfirmButton.discountPrice = Number($("#input-discount-price").val().trim());
    paramConfirmButton.duration = $("#input-duration").val().trim();
    paramConfirmButton.level = $("#input-level").val().trim();
    paramConfirmButton.coverImage = $("#input-course-image").val().trim();
    paramConfirmButton.teacherName = $("#input-teacher").val().trim();
    paramConfirmButton.teacherPhoto = $("#input-teacher-photo").val().trim();
    paramConfirmButton.isPopular = $("#select-popular").val().trim();
    paramConfirmButton.isTrending = $("#select-trending").val().trim();
}
// hàm validate thông tin thêm
function validateDataConfirm(paramConfirmButton) {
    var vObject = vObjectCourses
    if (paramConfirmButton.courseCode === "") {
        alert("chưa nhập Course code!");
        return false;
    }
    if (paramConfirmButton.courseName === "") {
        alert("chưa nhập Course Name!");
        return false;
    }
    if (paramConfirmButton.price === "" || paramConfirmButton.price < 0) {
        alert("chưa nhập giá tiền hoặc nhập sai");
        return false;
    }
    if (paramConfirmButton.discountPrice <= 0) {
        alert("Discount Price sai!");
        return false;
    }
    if (paramConfirmButton.duration === "") {
        alert("chưa nhập duration!");
        return false;
    }
    if (paramConfirmButton.level === "") {
        alert("chưa nhập level!");
        return false;
    }
    if (paramConfirmButton.coverImage === "") {
        alert("chưa nhập coverImage!");
        return false;
    }
    if (paramConfirmButton.teacherName === "") {
        alert("chưa nhập teacherName!");
        return false;
    }
    if (paramConfirmButton.teacherPhoto === "") {
        alert("chưa nhập teacherPhoto!");
        return false;
    }
    if (paramConfirmButton.isPopular === "no-pick") {
        alert("Vui lòng chọn isPopular!");
        return false;
    }
    if (paramConfirmButton.isTrending === "no-picktrend") {
        alert("Vui lòng chọn isTrending!");
        return false;
    }
    return true;
}
// hàm validate cho update function
function validateUpdateConfirm(paramConfirmButton) {
    var vObject = vObjectCourses
    if (paramConfirmButton.courseCode === "") {
        alert("chưa nhập Course code!");
        return false;
    }
    if (paramConfirmButton.courseName === "") {
        alert("chưa nhập Course Name!");
        return false;
    }
    if (paramConfirmButton.price === "" || paramConfirmButton.price < 0) {
        alert("chưa nhập giá tiền hoặc nhập sai");
        return false;
    }
    if (paramConfirmButton.discountPrice <= 0) {
        alert("Discount Price sai!");
        return false;
    }
    if (paramConfirmButton.duration === "") {
        alert("chưa nhập duration!");
        return false;
    }
    if (paramConfirmButton.level === "") {
        alert("chưa nhập level!");
        return false;
    }
    if (paramConfirmButton.coverImage === "") {
        alert("chưa nhập coverImage!");
        return false;
    }
    if (paramConfirmButton.teacherName === "") {
        alert("chưa nhập teacherName!");
        return false;
    }
    if (paramConfirmButton.teacherPhoto === "") {
        alert("chưa nhập teacherPhoto!");
        return false;
    }
    if (paramConfirmButton.isPopular === "no-pick") {
        alert("Vui lòng chọn isPopular!");
        return false;
    }
    if (paramConfirmButton.isTrending === "no-picktrend") {
        alert("Vui lòng chọn isTrending!");
        return false;
    }
    return true;
}
// update api
function callApiUpdate(paramConfirmButtonUpdate) {
    console.log(vIdCourseEdit);
    $.ajax({
        type: "PUT",
        async:false,
        url: "/courses/" + vIdCourseEdit,
        dataType: "json",
        data: JSON.stringify(paramConfirmButtonUpdate),
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            console.log(data);
            location.reload();
        },
        error: function (error) {
            alert(error.responseText);
        },
    })
}
// function xóa trắng bảng create
function resetCreateModal() {
    $("#input-id").val("");
    $("#input-course-code").val("");
    $("#input-course-name").val("");
    $("#input-price").val("");
    $("#input-discount-price").val("");
    $("#input-duration").val("");
    $("#input-level").val("");
    $("#input-course-image").val("");
    $("#input-teacher").val("");
    $("#input-teacher-photo").val("");
    $("#select-popular").val("no-pick");
    $("#select-trending").val("no-picktrend");
}
// load data lên update modal
function loadDataToUpdateModal(paramUpdate) {
    var vCurrentTr = $(paramUpdate).closest("tr");
    var vTable = $("#courses-table").DataTable();
    var vDataRow = vTable.row(vCurrentTr).data();
    vCourseCode = vDataRow.courseCode;
    console.log(vCourseCode);
    vObjectCourses.filter(item => {
        if (item.courseCode.toUpperCase()=== vCourseCode.toUpperCase()) {
            vIdCourseEdit = item._id;
            console.log(vIdCourseEdit);
        }
    })
    $("#input-update-course-code").val(vDataRow.courseCode);
    $("#input-update-course-name").val(vDataRow.courseName);
    $("#input-update-price").val(vDataRow.price);
    $("#input-update-discount-price").val(vDataRow.discountPrice);
    $("#input-update-duration").val(vDataRow.duration);
    $("#input-update-level").val(vDataRow.level);
    $("#input-update-image").val(vDataRow.coverImage);
    $("#input-update-teacher").val(vDataRow.teacherName);
    $("#input-update-photo").val(vDataRow.teacherPhoto);
    $("#select-update-popular").val(String(vDataRow.isPopular));
    $("#select-update-trending").val(String(vDataRow.isTrending));
}
// đọc dữ liệu update modal
function readDataUpdate(paramConfirmButtonUpdate) {
    paramConfirmButtonUpdate._id = vIdCourseEdit;
    paramConfirmButtonUpdate.courseCode = $("#input-update-course-code").val();
    paramConfirmButtonUpdate.courseName = $("#input-update-course-name").val();
    paramConfirmButtonUpdate.price = $("#input-update-price").val();
    paramConfirmButtonUpdate.discountPrice = $("#input-update-discount-price").val();
    paramConfirmButtonUpdate.duration = $("#input-update-duration").val();
    paramConfirmButtonUpdate.level = $("#input-update-level").val();
    paramConfirmButtonUpdate.coverImage = $("#input-update-image").val();
    paramConfirmButtonUpdate.teacherName = $("#input-update-teacher").val();
    paramConfirmButtonUpdate.teacherPhoto = $("#input-update-photo").val();
    paramConfirmButtonUpdate.isPopular = $("#select-update-popular").val();
    paramConfirmButtonUpdate.isTrending = $("#select-update-trending").val();
}
// xóa trắng update modal
function resetUpdateModal() {
    $("#input-id-edit").val("");
    $("#input-update-course-code").val("");
    $("#input-update-course-name").val("");
    $("#input-update-price").val("");
    $("#input-update-discount-price").val("");
    $("#input-update-duration").val("");
    $("#input-update-level").val("");
    $("#input-update-image").val("");
    $("#input-update-teacher").val("");
    $("#input-update-photo").val("");
    $("#select-update-popular").val("no-pick");
    $("#select-update-trending").val("no-picktrend");
}
// hàm sửa thông tin khóa học đã sửa vào obj
function updateDataCourse(paramConfirmButtonUpdate) {
    for (var i = 0; i < vObjectCourses.length; i++) {
        if (vObjectCourses[i].id === paramConfirmButtonUpdate.id) {
            vObjectCourses[i].courseCode = paramConfirmButtonUpdate.courseCode;
            vObjectCourses[i].courseName = paramConfirmButtonUpdate.courseName;
            vObjectCourses[i].price = paramConfirmButtonUpdate.price;
            vObjectCourses[i].discountPrice = paramConfirmButtonUpdate.discountPrice;
            vObjectCourses[i].duration = paramConfirmButtonUpdate.duration;
            vObjectCourses[i].level = paramConfirmButtonUpdate.level;
            vObjectCourses[i].coverImage = paramConfirmButtonUpdate.coverImage;
            vObjectCourses[i].teacherName = paramConfirmButtonUpdate.teacherName;
            vObjectCourses[i].teacherPhoto = paramConfirmButtonUpdate.teacherPhoto;
            vObjectCourses[i].isPopular = paramConfirmButtonUpdate.isPopular;
            vObjectCourses[i].isTrending = paramConfirmButtonUpdate.isTrending;
        }
    }
}
// load to table
function loadDataToTable(paramCoursesDB) {
    var vTable = $("#courses-table").DataTable();
    vTable.clear();
    vTable.rows.add(paramCoursesDB);
    vTable.draw();
}